# Places™ - Hybrid Positioning System (HPS)

```
  _____  _                      _____ _____  _  __
 |  __ \| |                    / ____|  __ \| |/ /
 | |__) | | __ _  ___ ___  ___| (___ | |  | | ' / 
 |  ___/| |/ _` |/ __/ _ \/ __|\___ \| |  | |  <  
 | |    | | (_| | (_|  __/\__ \____) | |__| | . \ 
 |_|    |_|\__,_|\___\___||___/_____/|_____/|_|\_\
                                                  
```

Places™ is a platform that provides high-accuracy indoor and outdoor location data without relying on beacons or sensors at physical point-of-sales.

## Android™ compatibility

|CODENAME          |VERSION    |KERNEL                      |RELEASE           |API    |
|------------------|-----------|----------------------------|------------------|-------|
|Petit Four        |1.1        |                     2.6    |  February 9, 2009|      2|
|Cupcake           |1.5        |                     2.6.27 |    April 27, 2009|      3|
|Donut             |1.6        |                     2.6.29 |September 15, 2009|      4|
|Eclair	           |2.0 – 2.1  |                     2.6.29 |  October 26, 2009|  5 – 7|
|Froyo             |2.2 – 2.2.3|                     2.6.32 |      May 20, 2010|      8|
|Gingerbread       |2.3 – 2.3.7|                     2.6.35 |  December 6, 2010| 9 – 10|
|Honeycomb         |3.0 – 3.2.6|                     2.6.36 | February 22, 2011|11 – 13|
|Ice Cream Sandwich|4.0 – 4.0.4|                     3.0.1  |  October 18, 2011|14 – 15|
|Jelly Bean        |4.1 – 4.3.1|           3.0.31 to 3.4.39 |      July 9, 2012|16 – 18|
|KitKat            |4.4 – 4.4.4|                     3.10   |  October 31, 2013|19 – 20|
|Lollipop          |5.0 – 5.1.1|                     3.16   | November 12, 2014|21 – 22|
|Marshmallow       |6.0 – 6.0.1|                     3.18   |   October 5, 2015|     23|
|Nougat            |7.0 – 7.1.2|                     4.4    |   August 22, 2016|24 – 25|
|Oreo              |8.0 – 8.1.0|                     4.10   |   August 21, 2017|26 – 27|
|Pie               |9.0        |4.4.107, 4.9.84, and 4.14.42|    August 6, 2018|     28|


## 3-steps to integrate the SDK to App

### 1. add the repository to the project `build.gradle`
```
allprojects {
    repositories {
        .
        .
        .
        maven {
            url "https://bitbucket.org/geeksys/places-android-sdk/raw/master"
        }
    }
}
```

### 2. add the dependency to the module `build.gradle`
```
dependencies {
    implementation 'br.com.geeksys.places:places-android-sdk:2.6.3'
}
```


### 3. configure the options and initialize the SDK
```
import br.com.geeksys.places.sdk.core.Places;
import br.com.geeksys.places.sdk.core.PlacesOpts;

public class MainActivity extends AppCompatActivity {
   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);

      PlacesOpts placesOpts = PlacesOpts.getInstance(MainActivity.this);
      // privacy
      placesOpts.setRequiresUserPrivacyConsent(true);

      // signals
      placesOpts.setUseGps(true, false);
      placesOpts.setUseWifi(true, false);
      placesOpts.setUseBluetooth(false, false);

      // sync
      placesOpts.setUseWifiData(true);
      placesOpts.setUseMobileData(false);

      // service
      placesOpts.setIsWifiLockEnabled(true);
      placesOpts.setIsWakefulReceiverEnabled(true);
      placesOpts.setIsForegroundServiceEnabled(true);
      placesOpts.setIsForegroundServiceStealthNotificationEnabled(true);

      // tracking
      placesOpts.setIsStationaryDevice(false);
      placesOpts.setIsReactiveTrackingEnabled(true);

      // logging
      placesOpts.setIsLoggingEnabled(true);

      boolean init = Places.init(MainActivity.this, placesOpts);

      if (init) {
         // Places.init() => success
      } else {
         // Places.init() => error
      }
   }
}
```

By default the SDK requests the following permissions:
```
<uses-permission android:name="android.permission.INTERNET"/>
<uses-permission android:name="android.permission.ACCESS_WIFI_STATE"/>
<uses-permission android:name="android.permission.CHANGE_WIFI_STATE"/>
<uses-permission android:name="android.permission.ACCESS_NETWORK_STATE"/>
<uses-permission android:name="android.permission.ACCESS_FINE_LOCATION"/>
<uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION"/>
<uses-permission android:name="android.permission.WAKE_LOCK"/>
<uses-permission android:name="android.permission.RECEIVE_BOOT_COMPLETED"/>
<uses-permission android:name="android.permission.FOREGROUND_SERVICE"/>
```


```

### by GeekSys
```
Luiz Vitor Martinez Cardoso <luiz.martinez@geeksys.com.br>
```
